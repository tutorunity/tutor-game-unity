﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPotionScript : MonoBehaviour {

    private float value = 1;

    public float GetValue()
    {
        return value;
    }

    public void Kill()
    {
        Destroy(gameObject);
    }

}
