﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item Scheme", menuName = "New Item Scheme/Default")]
public class NewItemSchemeScript : ScriptableObject {

    public new string name = "Default Item";
    public string description = "Write your description here.";

    public Sprite graphics;

    public float value;
}
