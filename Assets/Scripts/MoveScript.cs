﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Test enum for dropdown in editor
public enum OPTIONS
{
    CUBE = 0,
    SPHERE = 1,
    PLANE = 2
}

public class MoveScript : MonoBehaviour {

    // Test enum for dropdown in editor
    [Header("ENUM TEST")]
    public OPTIONS dropdown;

    [Space(10)]
    [Header("Movement")]
    [Range(1,40)]
    public float speed;
    public float horizontal = 0;
    public float vertical = 0;
    public float sprint = 0;

    [Space(10)]
    [Header("Abilities")]
    public float health = 1;
    public float energy = 10;
    [SerializeField]
    [Tooltip("If player is out of energy...")]
    private bool tired = false;

    [Space(20)]
    [Header("Properties")]
    public float maxHealth = 10;
    public float maxEnergy = 10;

    [Space(30)]
    [Header("Positions")]
    public float distTraveled = 0;

    [Header("UI elements")]
    public Text healthText;
    public Text energyText;
    public Text posXText;
    public Text posYText;
    public GameObject healthPanel;
    public GameObject energyPanel;
    private RectTransform healthBar;
    private RectTransform energyBar;

    // Use this for initialization
    void Start ()
    {
        healthBar = healthPanel.GetComponent<RectTransform>();
        energyBar = energyPanel.GetComponent<RectTransform>();

        healthBar.transform.localScale = new Vector3(health / maxHealth, 1, 1);
        healthText.text = health.ToString("F2");

        energyBar.transform.localScale = new Vector3(energy / maxEnergy, 1, 1);
        energyText.text = energy.ToString("F2");
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 posA = transform.position;
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
        sprint = Input.GetKey(KeyCode.LeftShift) && !tired ? 2 : 1;
        Vector3 moveVector = new Vector3(horizontal, vertical, 0).normalized * speed * sprint * Time.deltaTime;

        // If left shift is released or player is stopped, the energy starts to increase 
        if (Input.GetKeyUp(KeyCode.LeftShift) || moveVector == Vector3.zero) tired = false;

        // 
        if (sprint == 2 && (moveVector != Vector3.zero))
        {
            if (energy > 0f)
            {
                energy -= Time.deltaTime * 3;
            }
            if (energy <= 0)
            {
                energy = 0f;
                tired = true;
            }
            energyBar.transform.localScale = new Vector3(energy / maxEnergy, 1, 1);
            energyText.text = energy.ToString("F2");
        }
        else if(!tired)
        {
            if (energy < maxEnergy)
            {
                energy += Time.deltaTime * 1;
            }
            else
            {
                energy = maxEnergy;
            }
            energyBar.transform.localScale = new Vector3(energy / maxEnergy, 1, 1);
            energyText.text = energy.ToString("F2");
        }

        gameObject.transform.Translate(moveVector);

        // Distance traveled calculate
        Vector3 posB = transform.position;
        distTraveled += Vector3.Distance(posA, posB)/10;

        // Show positions
        ShowPositions();
    }

    private void ShowPositions()
    {
        posXText.text = transform.position.x.ToString("F2");
        posYText.text = transform.position.y.ToString("F2");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "HealthPotion" && health < maxHealth)
        {
            float m_value = collision.gameObject.GetComponent<HealthPotionScript>().GetValue();
            SetHealth(m_value);
            collision.gameObject.GetComponent<HealthPotionScript>().Kill();
        }
    }

    private void SetHealth(float value)
    {
        health += value;

        healthBar.transform.localScale = new Vector3(health / maxHealth, 1, 1);
        healthText.text = health.ToString("F2");
    }
}
