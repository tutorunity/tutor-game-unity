﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class framesPerSecScript : MonoBehaviour {

    Text fpsText;

    public float currentFPS;
    float displayFPSPerSec = 1; //ennyi másodpercenként írja ki az fpst
    float x;

    // Use this for initialization
    void Start () {
        fpsText = gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        currentFPS = 1 / Time.deltaTime;
        x -= Time.deltaTime;

        if (x <= 0)
        {
            fpsText.text = /*Mathf.RoundToInt(currentFPS).ToString()*/ currentFPS.ToString("F2");
            x = displayFPSPerSec;
        }
    }
}
